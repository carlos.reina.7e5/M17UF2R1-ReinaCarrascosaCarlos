# M17UF2R1-ReinaCarrascosaCarlos
## Segona entrega
### Controls
Moviment: WASD  
Apuntar: Ratolí  
Disparar: Click esquerra  
Dash: Espai  
Canvi d'arma: Roda del ratolí  
### Fitxa técnica
Aquest joc és un prototip d'un RogueLike Shooter amb vista Top-Down. Quan comences una partida, apareixeràs en una de les quatre sales possibles. A la pantalla tindràs informació sobre els enemics restants, la oleada actual, l'oleada final entre parèntesis, la teva arma, la munició, la vida, la puntuació i el cooldown del dash. A les oleades apareixeràn dos tipus d'enemics: crancs i torretes. Els primers et perseguiràn i els segons et dispararàn desde una posició fixa. Hauràs d'acabar amb ells amb qualsevol de les tres armes disponibles: pistola, rifle i uzi (comparteixen munició). Cada enemic mort te una possibilitat de dropejar un kit de salut o un kit de munició. Quan tots els enemics de la última oleada siguin eliminats, dues portes s'obriran. En entrar en una de les portes, aniràs a un altre nivell aleatori dels 4 possibles. Quan completis 3 nivells diferents, guanyaràs.  
Génere/s: RogueLike, Top-Down Shooter  
Tema/es: Dungeon Crawler, Fantasia, Militar, Arcade