using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public int LevelCount = 0;
    public int Score;

    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance is null) Debug.Log("Game Manager is NULL");

            return _instance;
        }
    }

    public bool AllEnemiesDown { get; set; }
    public enum PlayerState
    {
        Alive,
        Dead,
        Invencible,
        Paused
    }

    public PlayerState currentState;

    private void Awake()
    {
        if (_instance != null) Destroy(gameObject);
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (currentState == PlayerState.Dead && SceneManager.GetActiveScene().name != "GameOverScene" && SceneManager.GetActiveScene().name != "GameStartScene")
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }
}
