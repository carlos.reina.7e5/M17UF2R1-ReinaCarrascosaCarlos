using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = System.Random;

public class UIManager : MonoBehaviour
{
    private Text _resultText;
    private Text _waveCountText;
    private Text _enemyCountText;
    private Text _ammoCountText;
    private Text _dashCooldownText;
    private Text _weaponText;
    private Text _scoreText;
    private Button _playAgainButton;
    private Button _exitButton;
    private Button _playButton;
    private Button _configButton;

    // Start is called before the first frame update
    void Start()
    {
        if (SceneManager.GetActiveScene().name != "GameOverScene" && SceneManager.GetActiveScene().name != "GameStartScene") GameSceneStart();
        else if (SceneManager.GetActiveScene().name == "GameStartScene") GameStartSceneStart();
        else GameOverSceneStart();
    }

    void GameStartSceneStart()
    {
        _playButton = GameObject.Find("PlayButton").GetComponent<Button>();
        _playButton.onClick.AddListener(PlayButton);
        _configButton = GameObject.Find("ConfigButton").GetComponent<Button>();
        _configButton.onClick.AddListener(ConfigButton);
        _exitButton = GameObject.Find("ExitButton").GetComponent<Button>();
        _exitButton.onClick.AddListener(ExitButton);
    }

    void GameSceneStart()
    {
        _waveCountText = GameObject.Find("WaveCounter").GetComponent<Text>();
        _enemyCountText = GameObject.Find("EnemyCounter").GetComponent<Text>();
        _ammoCountText = GameObject.Find("Ammo").GetComponent<Text>();
        _dashCooldownText = GameObject.Find("Dash").GetComponent<Text>();
        _weaponText = GameObject.Find("Weapon").GetComponent<Text>();
        _scoreText = GameObject.Find("Score").GetComponent<Text>();
    }

    void GameOverSceneStart()
    {
        _playAgainButton = GameObject.Find("PlayAgainButton").GetComponent<Button>();
        _playAgainButton.onClick.AddListener(PlayAgainButton);
        _exitButton = GameObject.Find("ExitButton").GetComponent<Button>();
        _exitButton.onClick.AddListener(ExitButton);
        _resultText = GameObject.Find("ResultText").GetComponent<Text>();
        _scoreText = GameObject.Find("Score").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().name != "GameOverScene" && SceneManager.GetActiveScene().name != "GameStartScene") GameSceneUpdate();
        else if (SceneManager.GetActiveScene().name == "GameStartScene") GameStartSceneUpdate();
        else GameOverSceneUpdate();
    }

    void GameStartSceneUpdate()
    {

    }

    void GameSceneUpdate()
    {
        ChangeWaveCount();
        ChangeEnemiesCount();
        ChangeAmmo();
        ChangeDashCooldown();
        ChangeWeapon();
        ChangeScore();
    }

    void GameOverSceneUpdate()
    {
        ChangeResultText();
        ChangeScoreGameOver();
    }

    void ChangeWaveCount()
    {
        _waveCountText.text = "Wave: " + EnemySpawner.currWave + "(" + EnemySpawner.finalWave + ")";
    }

    void ChangeEnemiesCount()
    {
        _enemyCountText.text = "Enemies left: " + EnemySpawner.EnemiesCount;
    }

    void ChangeAmmo()
    {
        _ammoCountText.text = "Ammo: " + PlayerController.Ammo;
    }

    void ChangeDashCooldown()
    {
        int _cooldown = Convert.ToInt32(PlayerController.dashCooldownCounter); 
        _dashCooldownText.text = "Dash cooldown: " + _cooldown;
    }

    void ChangeWeapon()
    {
        _weaponText.text = "Weapon: " + PlayerController.Weapon;
    }

    void ChangeScore()
    {
        _scoreText.text = "Score: " + GameManager.Instance.Score;
    }

    void ChangeResultText()
    {
        if (GameManager.Instance.currentState == GameManager.PlayerState.Alive) _resultText.text = "Congratulations!";
        else _resultText.text = "Better luck next time";
    }

    void ChangeScoreGameOver()
    {
        _scoreText.text = "Your final score is " + GameManager.Instance.Score + " points";
    }

    void PlayButton()
    {
        Random rnd = new Random();
        SceneManager.LoadScene(rnd.Next(1, 5));
    }

    void ConfigButton()
    {

    }

    void PlayAgainButton()
    {
        Random rnd = new Random();
        SceneManager.LoadScene(rnd.Next(1, 5)); ;
        GameManager.Instance.AllEnemiesDown = false;
        GameManager.Instance.currentState = GameManager.PlayerState.Alive;
        GameManager.Instance.LevelCount = 0;
        EnemySpawner.currWave = 0;
        EnemySpawner.EnemiesCount = 0;
    }

    void ExitButton()
    {
        Application.Quit(); // this works when the game is built and it's a desktop application
        UnityEditor.EditorApplication.isPlaying = false; // this quits the editor play mode
        Debug.Log("Application quitting...");
    }
}
