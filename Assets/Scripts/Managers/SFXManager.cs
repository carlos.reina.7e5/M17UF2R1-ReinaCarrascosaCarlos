using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXManager : MonoBehaviour
{

    public AudioSource Audio;
    public AudioClip Shoot;
    public AudioClip Death;
    public AudioClip Hit;
    public AudioClip HealthKit;
    public AudioClip AmmoKit;
    public AudioClip DoorOpen;
    public AudioClip Dash;

    public static SFXManager instance;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(this);
    }
}
