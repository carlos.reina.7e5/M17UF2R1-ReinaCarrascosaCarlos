using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour, IDamageable
{

    [SerializeField] protected int _hp;
    [SerializeField] protected float _speed;
    protected Animator _animator;
    protected Rigidbody2D _rigidbody;
    [SerializeField] protected Transform FirePoint;
    [SerializeField] protected GameObject Bullet;
    [SerializeField] protected float BulletForce;
    protected float fireRate;
    protected float lastShot;
    public static int Ammo;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        _animator = GetComponent<Animator>();
    }

    protected virtual void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        
    }

    protected virtual void FixedUpdate()
    {

    }

    protected virtual void Shoot(int ammo)
    {
        if (Time.time > fireRate + lastShot && ammo > 0)
        {
            SFXManager.instance.Audio.PlayOneShot(SFXManager.instance.Shoot);
            GameObject bullet = Instantiate(Bullet, FirePoint.position, FirePoint.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(FirePoint.up * BulletForce, ForceMode2D.Impulse);
            lastShot = Time.time;
            if (Ammo > 0) Ammo--;
        }
    }

    public void ApplyDamage(int damageTaken)
    {
        _hp -= damageTaken;
        SFXManager.instance.Audio.PlayOneShot(SFXManager.instance.Hit);
        if (_hp <= 0)
        {
            Destroy(gameObject);
            SFXManager.instance.Audio.PlayOneShot(SFXManager.instance.Death);
            GameManager.Instance.Score += 5;
        }
        if (_hp <= 0 && gameObject.name == "Turret Gun")
        {
            Destroy(gameObject);
            Destroy(transform.parent.gameObject);
            GameManager.Instance.Score += 5;
        }
        if (_hp <= 0 && gameObject.name == "Player")
        {
            Destroy(GameObject.Find("BgMusic"));
            Destroy(gameObject);
            GameManager.Instance.currentState = GameManager.PlayerState.Dead;
        }
    }
}
