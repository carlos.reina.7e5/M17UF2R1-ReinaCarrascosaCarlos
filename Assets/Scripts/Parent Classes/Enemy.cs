using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Enemy : Character
{

    protected Transform _target;
    protected Vector2 _direction;
    public GameObject[] ItemDrops;
    protected const float _dropChance = 1f / 3f;  // 1 in 4 chance.
    protected bool isQuitting = false;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        _target = GameObject.Find("Player").transform;
    }

    void OnApplicationQuit()
    {
        isQuitting = true;
    }

    // Update is called once per frame
    protected override void Update()
    {
        LookPlayer();
    }

    protected override void FixedUpdate()
    {
        if (_target)
        {
            _rigidbody.velocity = new Vector2(_direction.x, _direction.y) * _speed;
        }
    }

    protected virtual void LookPlayer()
    {
        if (_target)
        {
            Vector3 direction = (_target.position - transform.position).normalized;
            float angle = Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg;
            _rigidbody.rotation = angle;
            _direction = direction;
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        IDamageable damageable = collision.GetComponent<IDamageable>();
        if (damageable != null && !collision.CompareTag("Enemy") && GameManager.Instance.currentState != GameManager.PlayerState.Invencible)
        {
            damageable.ApplyDamage(2);
        }
    }

    protected virtual void OnDestroy()
    {
        if (GameObject.FindGameObjectWithTag("Spawner") != null)
        {
            GameObject.FindGameObjectWithTag("Spawner").GetComponent<EnemySpawner>().spawnedEnemies.Remove(gameObject);
            EnemySpawner.EnemiesCount--;
        }

        if (!isQuitting && GameManager.Instance.currentState != GameManager.PlayerState.Dead)
            if (Random.Range(0f, 1f) <= _dropChance) ItemDrop();
    }

    protected override void Shoot(int ammo)
    {
        if (Time.time > fireRate + lastShot)
        {
            SFXManager.instance.Audio.PlayOneShot(SFXManager.instance.Shoot);
            _animator.SetTrigger("shoot");
            GameObject bullet = Instantiate(Bullet, FirePoint.position, FirePoint.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(FirePoint.up * BulletForce, ForceMode2D.Impulse);
            lastShot = Time.time;
        }
    }

    protected virtual void ItemDrop()
    {
        int RandomNumber = Random.Range(0, 2);
        Instantiate(ItemDrops[RandomNumber], gameObject.transform.position, Quaternion.identity);
    }
}
