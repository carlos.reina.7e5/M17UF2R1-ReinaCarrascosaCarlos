using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonMusicController : MonoBehaviour
{
    public static DungeonMusicController instance;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(this);
    }
}
