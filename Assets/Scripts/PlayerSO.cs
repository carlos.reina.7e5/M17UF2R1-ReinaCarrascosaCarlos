using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Player", menuName ="ScriptableObjects/Player")]
public class PlayerSO : ScriptableObject
{
    public string Name;
    public int Health;
    public int Score;
}
