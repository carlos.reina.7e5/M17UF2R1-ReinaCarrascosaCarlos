using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBulletController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        IDamageable damageable = collision.GetComponent<IDamageable>();
        if (damageable != null && !collision.CompareTag("Player"))
        {
            damageable.ApplyDamage(1);
        }
        if (!collision.CompareTag("Player") && !collision.CompareTag("Bullet") && !collision.CompareTag("Enemy Bullet") && !collision.CompareTag("Health Kit") && !collision.CompareTag("Ammo")) Destroy(gameObject);
    }
}
