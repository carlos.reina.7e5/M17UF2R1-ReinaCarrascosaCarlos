using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Character
{

    private Vector2 _input;
    public int maxHealth = 10;
    public PlayerHealthBarController healthBar;

    private float activeMoveSpeed;
    public float dashSpeed;

    public float dashLength = .5f, dashCooldown = 2f;

    private float dashCounter;
    public static float dashCooldownCounter;

    public static string Weapon = "Pistol";

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        GameManager.Instance.currentState = GameManager.PlayerState.Alive;

        fireRate = 0.5f;
        Ammo = 100;
        _hp = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        activeMoveSpeed = _speed;
    }

    // Update is called once per frame
    protected override void Update()
    {
        ChangeWeapon();
        if (Input.GetButtonDown("Fire1") && GameManager.Instance.currentState != GameManager.PlayerState.Paused)
        {
            Shoot(Ammo);
            _animator.SetTrigger("shoot");
        }

        if (GameManager.Instance.currentState != GameManager.PlayerState.Paused) LookMouse();
        if (_input == new Vector2(0, 0))
        {
            _animator.SetBool("isWalking", false);
        }
        else
        {
            _animator.SetBool("isWalking", true);
        }

        Dash();
    }

    void ChangeWeapon()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
        {
            if (fireRate == 0.5f)
            {
                fireRate = 0.25f;
                Weapon = "Rifle";
            }
            else if (fireRate == 0.25f)
            {
                fireRate = 0;
                Weapon = "Uzi";
            }
            else if (fireRate == 0)
            {
                fireRate = 0.5f;
                Weapon = "Pistol";
            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
        {
            if (fireRate == 0.5f)
            {
                fireRate = 0;
                Weapon = "Uzi";
            }
            else if (fireRate == 0.25f)
            {
                fireRate = 0.5f;
                Weapon = "Pistol";
            }
            else if (fireRate == 0)
            {
                fireRate = 0.25f;
                Weapon = "Rifle";
            }
        }
    }

    protected override void FixedUpdate()
    {
        Movement();
    }

    void LookMouse()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.up = mousePos - new Vector2(transform.position.x, transform.position.y);
    }

    void Movement()
    {
        _input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        _rigidbody.velocity = _input.normalized * activeMoveSpeed;
    }

    void Dash()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (dashCooldownCounter <= 0 && dashCounter <= 0)
            {
                SFXManager.instance.Audio.PlayOneShot(SFXManager.instance.Dash);
                activeMoveSpeed = dashSpeed;
                dashCounter = dashLength;
                GameManager.Instance.currentState = GameManager.PlayerState.Invencible;
            }
        }

        if (dashCounter > 0)
        {
            dashCounter -= Time.deltaTime;

            if (dashCounter <= 0)
            {
                activeMoveSpeed = _speed;
                dashCooldownCounter = dashCooldown;
                GameManager.Instance.currentState = GameManager.PlayerState.Alive;
            }
        }

        if (dashCooldownCounter > 0)
        {
            dashCooldownCounter -= Time.deltaTime;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy") || collision.CompareTag("Enemy Bullet"))
        {
            healthBar.SetHealth(_hp);
        }

        if (collision.CompareTag("Health Kit"))
        {
            if (_hp < 9) _hp += 2;
            else if (_hp == 9) _hp += 1;
            healthBar.SetHealth(_hp);
        }

        if (collision.CompareTag("Ammo"))
        {
            Ammo += 10;
            if (Ammo > 100) Ammo = 100;
        }
    }
}
