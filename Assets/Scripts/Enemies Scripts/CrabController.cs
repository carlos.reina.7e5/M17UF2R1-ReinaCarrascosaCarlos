using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrabController : Enemy
{
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        _hp = 3;
        _speed = 2;
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D (collision);
        if (collision.CompareTag("Player") && GameManager.Instance.currentState != GameManager.PlayerState.Invencible)
        {
            SFXManager.instance.Audio.PlayOneShot(SFXManager.instance.Death);
            Destroy(gameObject);
        }
    }
}
