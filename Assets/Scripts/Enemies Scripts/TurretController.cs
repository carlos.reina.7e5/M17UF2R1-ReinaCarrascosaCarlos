using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class TurretController : Enemy
{
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        _hp = 5;
        _speed = 0;
        fireRate = 1.5f;
        BulletForce = 10;
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        Shoot(1);
    }

    protected override void OnDestroy()
    {
        if (GameObject.FindGameObjectWithTag("Spawner") != null)
        {
            GameObject.FindGameObjectWithTag("Spawner").GetComponent<EnemySpawner>().spawnedEnemies.Remove(transform.parent.gameObject);
            EnemySpawner.EnemiesCount--;
        }

        if (!isQuitting && GameManager.Instance.currentState != GameManager.PlayerState.Dead)
            if (Random.Range(0f, 1f) <= _dropChance) ItemDrop();
    }
}
