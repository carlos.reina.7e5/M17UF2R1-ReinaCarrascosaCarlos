using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemySpawner : MonoBehaviour
{

    public List<SpawnedEnemy> enemies = new List<SpawnedEnemy>();
    public static int currWave;
    private int waveValue;
    public static int finalWave;
    private int difficultValue;
    public List<GameObject> enemiesToSpawn = new List<GameObject>();
    public static int EnemiesCount;

    public Transform[] spawnLocation;
    public int spawnIndex;

    public int waveDuration;
    private float waveTimer;
    private float spawnInterval;
    private float spawnTimer;

    public List<GameObject> spawnedEnemies = new List<GameObject>();


    // Start is called before the first frame update
    void Start()
    {
        GenerateWave();
        if (SceneManager.GetActiveScene().name == "GameScene")
        {
            finalWave = 2;
            difficultValue = 7;
        }
        if (SceneManager.GetActiveScene().name == "GameScene2")
        {
            finalWave = 3;
            difficultValue = 5;
        }
        if (SceneManager.GetActiveScene().name == "GameScene3")
        {
            finalWave = 2;
            difficultValue = 9;
        }
        if (SceneManager.GetActiveScene().name == "GameScene4")
        {
            finalWave = 4;
            difficultValue = 3;
        }
    }

    private void FixedUpdate()
    {
        if (spawnTimer <= 0)
        {
            //spawn an enemy
            if (enemiesToSpawn.Count > 0)
            {
                GameObject enemy = (GameObject)Instantiate(enemiesToSpawn[0], spawnLocation[spawnIndex].position, Quaternion.identity); // spawn first enemy in our list
                enemiesToSpawn.RemoveAt(0); // and remove it
                spawnedEnemies.Add(enemy);
                spawnTimer = spawnInterval;

                if (spawnIndex + 1 <= spawnLocation.Length - 1)
                {
                    spawnIndex++;
                }
                else
                {
                    spawnIndex = 0;
                }
            }
            else
            {
                waveTimer = 0; // if no enemies remain, end wave
            }
        }
        else
        {
            spawnTimer -= Time.fixedDeltaTime;
            waveTimer -= Time.fixedDeltaTime;
        }

        if (waveTimer <= 0 && spawnedEnemies.Count <= 0 && currWave < finalWave) // only {finalWave} waves
        {
            currWave++;
            GenerateWave();
            EnemiesCount = enemiesToSpawn.Count;
        }

        if (currWave == finalWave && waveTimer <= 0 && spawnedEnemies.Count <= 0)
        {
            GameManager.Instance.AllEnemiesDown = true; // when enemies in wave {finalWave} are 0, all enemies are down
        }
    }

    public void GenerateWave()
    {
        waveValue = currWave * difficultValue;
        GenerateEnemies();

        if (enemiesToSpawn.Count > 0) spawnInterval = waveDuration / enemiesToSpawn.Count; // gives a fixed time between each enemies
        waveTimer = waveDuration; // wave duration is read only
    }

    public void GenerateEnemies()
    {
        // Create a temporary list of enemies to generate
        // 
        // selects a random enemy 
        // see if we can afford it
        // if we can, add it to our list, and reduce waveValue

        // repeat... 

        //  -> if we have no points left, leave the loop (no more enemies in that wave)

        List<GameObject> generatedEnemies = new List<GameObject>();
        while (waveValue > 0 || generatedEnemies.Count < 50)
        {
            int randEnemyId = Random.Range(0, enemies.Count);
            int randEnemyCost = enemies[randEnemyId].cost;

            if (waveValue - randEnemyCost >= 0)
            {
                generatedEnemies.Add(enemies[randEnemyId].enemyPrefab);
                waveValue -= randEnemyCost;
            }
            else if (waveValue <= 0)
            {
                break;
            }
        }
        enemiesToSpawn.Clear();
        enemiesToSpawn = generatedEnemies;
    }
}

[System.Serializable]
public class SpawnedEnemy
{
    public GameObject enemyPrefab;
    public int cost;
}
