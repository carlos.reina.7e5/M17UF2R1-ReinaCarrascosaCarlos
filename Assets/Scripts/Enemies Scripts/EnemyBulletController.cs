using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        IDamageable damageable = collision.GetComponent<IDamageable>();
        if (damageable != null && !collision.CompareTag("Enemy") && GameManager.Instance.currentState != GameManager.PlayerState.Invencible)
        {
            damageable.ApplyDamage(1);
        }
        if(!collision.CompareTag("Enemy") && !collision.CompareTag("Bullet") && !collision.CompareTag("Enemy Bullet") && !collision.CompareTag("Health Kit") && !collision.CompareTag("Ammo") && GameManager.Instance.currentState != GameManager.PlayerState.Invencible) Destroy(gameObject);
    }
}
