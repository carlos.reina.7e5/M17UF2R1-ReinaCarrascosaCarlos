using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;
using Random = System.Random;

public class DoorController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && GameManager.Instance.AllEnemiesDown)
        {
            GameManager.Instance.Score += 25;
            GameManager.Instance.LevelCount++;
            if (GameManager.Instance.LevelCount >= 3)
            {
                Destroy(GameObject.Find("BgMusic"));
                SceneManager.LoadScene("GameOverScene");
            }
            else
            {
                int x = PlayerController.Ammo;
                Random rnd = new Random();
                SceneManager.LoadScene(rnd.Next(1, 4));
                GameManager.Instance.AllEnemiesDown = false;
                GameManager.Instance.currentState = GameManager.PlayerState.Alive;
                EnemySpawner.currWave = 0;
                EnemySpawner.EnemiesCount = 0;
                PlayerController.Ammo = x;
            }
        }
    }

    private void Update()
    {
        if (GameManager.Instance.AllEnemiesDown)
        {
            StartCoroutine(Fade());
        }
    }

    IEnumerator Fade()
    {
        Tilemap tilemap = GameObject.Find("Grid").GetComponentInChildren<Tilemap>();
        if (SceneManager.GetActiveScene().name == "GameScene")
        {
            tilemap.SetTile(new Vector3Int(3, 3, 0), null);
            tilemap.SetTile(new Vector3Int(-9, 0, 0), null);
        }
        else if (SceneManager.GetActiveScene().name == "GameScene2")
        {
            tilemap.SetTile(new Vector3Int(0, 3, 0), null);
            tilemap.SetTile(new Vector3Int(-1, -4, 0), null);
        }
        else if (SceneManager.GetActiveScene().name == "GameScene3")
        {
            tilemap.SetTile(new Vector3Int(-1, -5, 0), null);
            tilemap.SetTile(new Vector3Int(7, 3, 0), null);
        }
        else if (SceneManager.GetActiveScene().name == "GameScene4")
        {
            tilemap.SetTile(new Vector3Int(3, 3, 0), null);
            tilemap.SetTile(new Vector3Int(-9, 0, 0), null);
        }
        SFXManager.instance.Audio.PlayOneShot(SFXManager.instance.DoorOpen);
        yield return null;
    }
}
